package com.example.encuesdam_firebase

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast

@SuppressLint("Registered")
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btn_testrapido        = findViewById<Button>(R.id.btn_testrapidos)
        //val btn_anadirdatos     = findViewById<Button>(R.id.btn_anadirdatos)
        val btn_anadirdatos02     = findViewById<Button>(R.id.btn_anadirdatos02)
        val btn_contestarEncuesta = findViewById<Button>(R.id.btn_contestarEncuesta)





        btn_testrapido.setOnClickListener {
            Toast.makeText(this, "Iniciando Test Rápido", Toast.LENGTH_LONG).show()
            startActivity(Intent(this@MainActivity, Categorias::class.java))
        }

        /*
        btn_anadirdatos.setOnClickListener {
            Toast.makeText(this, "Conectando con la BBDD", Toast.LENGTH_LONG).show()
            startActivity(Intent(this@MainActivity, AnadirDatos::class.java))
        }
        */

        btn_anadirdatos02.setOnClickListener {
            Toast.makeText(this, "Conectando con la BBDD", Toast.LENGTH_LONG).show()
            startActivity(Intent(this@MainActivity, AnadirDatos::class.java))
        }

        btn_contestarEncuesta.setOnClickListener {
            Toast.makeText(this, "Conectando con la BBDD", Toast.LENGTH_LONG).show()
            startActivity(Intent(this@MainActivity, LeerDatos::class.java))
        }

    }
}