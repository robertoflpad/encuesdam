package com.example.encuesdam_firebase

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import java.math.BigDecimal
import java.math.RoundingMode

class Mates_test : AppCompatActivity() {

    private var pregunta: TextView? = null      // Creamos las variables para la pregunta y la correccion
    private var respuesta: EditText? = null        // EditText --> La variable respuesta la escribira el usuario
    private var nota_caja: TextView? = null


    private var contador:Int = 0

    private var progressBar:ProgressBar? = null

    private var total_preguntas: Int = 6

    private var pregunta_actual: Int = 0
    private var npre: TextView? = null

    private var nota = 0

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.mates_test)

        pregunta    = findViewById(R.id.pre)    as TextView // Enlaza las variables con los elementos de la interfaz grafica
        respuesta   = findViewById(R.id.res)    as EditText
        nota_caja   = findViewById(R.id.nota)   as TextView

        npre  = findViewById(R.id.npre) as TextView
        npre?.text="$pregunta_actual / " + (total_preguntas-1)

        nuevaPregunta()

        val checkBtn = findViewById(R.id.com) as Button // Constante
        checkBtn.setOnClickListener {        // Cuando se pulse el botón "COMPROBAR"
            comprueba()                  // llama a la función comprueba()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun comprueba() {
        val user_resp_normal  = respuesta?.text.toString() // Crea la constante que almacena la respuesta del usuario
        val user_resp = user_resp_normal.toLowerCase()
        val correct_resp = pedir_respuesta(contador)

        respuesta?.setText("")

        progressBar = findViewById(R.id.progressBar) as ProgressBar
        pregunta_actual++
        npre?.text="$pregunta_actual / " + (total_preguntas-1)
        progressBar!!.progress += 1             // La barra de progreso aumenta en uno
        contador++

        if (user_resp == correct_resp) {
            nota++
        }

        nuevaPregunta()

        if ( pregunta_actual == total_preguntas-1 ) {
            var nota_final: Double = (nota * 10 ) / (total_preguntas-1).toDouble()
            val decimal = BigDecimal(nota_final).setScale(1, RoundingMode.HALF_EVEN)
            nota_caja?.text = "$decimal / 10"
        }
    }

    private fun nuevaPregunta() {
        pregunta?.text = pedir_cuestiones(contador)
    }

    private fun pedir_cuestiones(i : Int) : String {
        val cuestiones = arrayOf("4 x 4 - 4 + 4 x 4 = ¿...?",
            "Si x+3= y, ¿cuánto es 2x + 6?",
            "(x-4) = 18. ¿Cuál es el valor de X?",
            "¿A cuánto equivale π?",
            "¿Cuánto es la cuarta parte de la tercera parte?",
            "Test Finalizado")
        return cuestiones[i]
    }

    private fun pedir_respuesta(i : Int) : String {
        val soluciones = arrayOf("28",
            "2y",
            "10",
            "3.141592",
            "Un Doceavo")
        return soluciones[i]
    }

}
