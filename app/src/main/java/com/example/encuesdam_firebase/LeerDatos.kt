package com.example.encuesdam_firebase

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import android.widget.Toast
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_encuesta.*


@SuppressLint("Registered")
class LeerDatos : AppCompatActivity() {

    lateinit var pregunta_box: EditText     // Caja de texto del xml
    lateinit var resp1_box: EditText
    lateinit var resp2_box: EditText
    lateinit var resp3_box: EditText
    lateinit var resp4_box: EditText

    lateinit var btn_enviar: Button         // Botón enviar del xml

    lateinit var listView: ListView
    lateinit var heroList: MutableList<guardar_dato>
    lateinit var ref: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_encuesta)

        heroList = mutableListOf()
        ref = FirebaseDatabase.getInstance().getReference("Preguntas")


        pregunta_box = findViewById(R.id.pregunta)
        resp1_box = findViewById(R.id.op1)
        resp2_box = findViewById(R.id.op2)
        resp3_box = findViewById(R.id.op3)
        resp4_box = findViewById(R.id.op4)
        btn_enviar   = findViewById(R.id.buttonSave)
        listView = findViewById(R.id.listView)

        btn_enviar.setOnClickListener {
        }

        ref.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onDataChange(p0: DataSnapshot) {
                if (p0!!.exists()) {
                    for (h in p0.children) {
                        val hero = h.getValue(guardar_dato::class.java)
                        heroList.add(hero!!)
                    }

                    val adapter = HeroAdapter(applicationContext, R.layout.heroes, heroList)
                    listView.adapter = adapter
                }
            }
        })
    }
}