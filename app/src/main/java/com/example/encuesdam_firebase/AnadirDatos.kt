package com.example.encuesdam_firebase

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.database.*


@SuppressLint("Registered")
class AnadirDatos : AppCompatActivity() {

    lateinit var pregunta_box: EditText     // Caja de texto del xml
    lateinit var resp_correcta_box: EditText
    lateinit var resp2_box: EditText
    lateinit var resp3_box: EditText
    lateinit var resp4_box: EditText

    lateinit var btn_enviar: Button         // Botón enviar del xml


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.anadir_datos02)

        pregunta_box = findViewById(R.id.editTextName)
        resp_correcta_box = findViewById(R.id.answer_correct)
        resp2_box = findViewById(R.id.answer02)
        resp3_box = findViewById(R.id.answer03)
        resp4_box = findViewById(R.id.answer04)
        btn_enviar   = findViewById(R.id.buttonSave)

        btn_enviar.setOnClickListener {
            guardar_pregunta()
        }

    }

    private fun guardar_pregunta () {
        val pregunta      = pregunta_box.text.toString().trim()
        val resp_correcta = resp_correcta_box.text.toString().trim()
        val resp2         = resp2_box.text.toString().trim()
        val resp3         = resp3_box.text.toString().trim()
        val resp4         = resp4_box.text.toString().trim()

        if (pregunta.isEmpty()) {
            pregunta_box.error = "Introduzca una pregunta"
            return
        }

        if (resp_correcta.isEmpty() || resp2.isEmpty()) {
            resp_correcta_box.error = "Introduzca una respuesta correcta"
            resp2_box.error = "Introduzca una segunda respuesta"
            return
        }

        val ref = FirebaseDatabase.getInstance().getReference("Preguntas")
        val preguntaID = ref.push().key

        val registro = guardar_dato(preguntaID, pregunta, resp_correcta, resp2, resp3, resp4)

        ref.child(preguntaID!!).setValue(registro).addOnCompleteListener {
            Toast.makeText(applicationContext, "Pregunta insertada", Toast.LENGTH_LONG).show()
        }


    }

}