package com.example.encuesdam_firebase

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast

class Categorias : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.categorias)

        val btn_mates     = findViewById<Button>(R.id.btn_mates)
        val btn_geografia = findViewById<Button>(R.id.btn_geografia)
        val btn_historia  = findViewById<Button>(R.id.btn_historia)

        btn_mates.setOnClickListener {
            Toast.makeText(this, "Buena Suerte", Toast.LENGTH_LONG).show()
            startActivity(Intent(this@Categorias, Mates_test::class.java))
        }

        btn_geografia.setOnClickListener {
            Toast.makeText(this, "Buena Suerte", Toast.LENGTH_LONG).show()
            startActivity(Intent(this@Categorias, Geografia_test::class.java))
        }

        btn_historia.setOnClickListener {
            Toast.makeText(this, "Buena Suerte", Toast.LENGTH_LONG).show()
            startActivity(Intent(this@Categorias, Historia_test::class.java))
        }


    }
}