package com.example.encuesdam_firebase

import androidx.appcompat.app.AppCompatActivity
import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import java.math.BigDecimal
import java.math.RoundingMode

@SuppressLint("Registered")
class TestRapido : AppCompatActivity() {

    private var pregunta: TextView? = null      // Creamos las variables para la pregunta y la correccion
    private var respuesta: EditText? = null        // EditText --> La variable respuesta la escribira el usuario
    private var nota_caja: TextView? = null


    private var contador:Int = 0

    private var progressBar:ProgressBar? = null

    private var total_preguntas: Int = 6

    private var pregunta_actual: Int = 0
    private var npre: TextView? = null

    private var nota = 0


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.test_rapido)

        pregunta    = findViewById(R.id.pre)    as TextView // Enlaza las variables con los elementos de la interfaz grafica
        respuesta   = findViewById(R.id.res)    as EditText
        nota_caja   = findViewById(R.id.nota)   as TextView

        npre  = findViewById(R.id.npre) as TextView
        npre?.text="$pregunta_actual / " + (total_preguntas-1)

        nuevaPregunta()

        val checkBtn = findViewById(R.id.com) as Button // Constante
        checkBtn.setOnClickListener {        // Cuando se pulse el botón "COMPROBAR"
            comprueba()                  // llama a la función comprueba()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun comprueba() {
        val user_resp  = respuesta?.text.toString() // Crea la constante que almacena la respuesta del usuario
        val correct_resp = pedir_respuesta(contador)

        progressBar = findViewById(R.id.progressBar) as ProgressBar
        pregunta_actual++
        npre?.text="$pregunta_actual / " + (total_preguntas-1)
        progressBar!!.progress += 1             // La barra de progreso aumenta en uno
        contador++

        if (user_resp == correct_resp) {
            nota++
        }

        nuevaPregunta()

        if ( pregunta_actual == (total_preguntas -1) ) {
            var nota_final: Double = (nota * 10 ) / total_preguntas.toDouble()
            val decimal = BigDecimal(nota_final).setScale(2, RoundingMode.HALF_EVEN)
            nota_caja?.text = "Nota: $decimal"
        }
    }

    private fun nuevaPregunta() {
        pregunta?.text = pedir_cuestiones(contador)
    }

    private fun pedir_cuestiones(i : Int) : String {
        val cuestiones = arrayOf("2 + 5 * 3 * 0",
            "¿Cuál es el animal que tiene más dientes del mundo?",
            "¿Quién era el dios romano de la guerra?",
            "¿Qué parte del cuerpo produce insulina?",
            "¿Con quién pasó DAM San Valentín?",
            "Test Finalizado")
        return cuestiones[i]
    }

    private fun pedir_respuesta(i : Int) : String {
        val soluciones = arrayOf("2",
            "Caracol",
            "Marte",
            "Pancreas",
            "MariaDB")
        return soluciones[i]
    }

}